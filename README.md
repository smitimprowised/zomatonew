# Zomato Information Retrival

This is a set of "Zomto data retrival" I've found for writing JavaScript modules. This guide deals specifically with front- and back-end Node/CommonJS modules hosted on npm, but the same concepts may apply elsewhere. 

### contents

- [List of Restaurant which has Has_Online_delivery option](#basic-exercise)
- [Information about Restaurants which has maximum and minimum average cost for two]
- [Get the information about the 20 Restaurants for the city given in the argument]
- [List of Restaurant which has Dessert as a Cuisine in Ankara city]
- [Information about Restaurants which are in the range of Longitude and Latitude]


## How to run all the above programs ( All the programs written on Noide.js)

# run from terminal , go to the directory where zomato.csv file located 
1) node listres.js 
2) node minmaxavgcost.js
3) node cityname.js Pune ( Where Third argument is city name that you have to pass as command-line argument to the programme)
4) node restaurantlist.js
5) node rangeres.js 

